#!/bin/bash

# copy files to service account EOS
CERNUSER=umamibot

TARGET_SERVER=lxplus.cern.ch
TARGET_EOS=/eos/user/u/umamibot/www/ci/dump-cdi/output
TARGET_FILE=ci-output.tar.gz
# create TAR ball
tar -czvf ${TARGET_FILE} output/
scp ${TARGET_FILE} ${CERNUSER}@${TARGET_SERVER}:${TARGET_EOS}/
rm ${TARGET_FILE}
