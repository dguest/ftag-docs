from os import makedirs
from os.path import join

import ROOT
import yaml

ROOT.xAOD.Init().ignore()
ROOT.gROOT.ProcessLine(".L utils/btagtools.C")
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)
from ROOT import TFile  # noqa
from ROOT import getbtaginfo  # noqa

# TODO: move this to a class to avoid initialising of the efficiency tool every time the function is called
# TODO: other alternative to unshittify this part of the code:
#       use CDI file in a non-complicated way
#       https://gitlab.cern.ch/atlas-ftag-calibration/comb_CDI#extracting-informations-from-a-cdi-file
#       using from ROOT.Analysis import CalibrationDataHistogramContainer


def dumpEfficiencies(cdi_path, config):
    # pt values of reference jets to look up efficiency
    pt_low = config["evaluate_eff"]["pt_low"]
    pt_high = config["evaluate_eff"]["pt_high"]

    # taggers to scan
    selected_taggers = config["evaluate_eff"]["taggers"]
    # jet collections to scan
    selected_jet_collections = config["evaluate_eff"]["jet_collections"]

    # scan CDI file
    cdi_file = TFile(cdi_path, "r")
    result = {}
    for key in cdi_file.GetListOfKeys():
        if key.GetName() == "VersionInfo":
            continue
        tagger = key.GetName()
        # only dump efficiencies for selected taggers (in config.yaml)
        if tagger not in selected_taggers:
            continue
        result[tagger] = {}
        tagdir = cdi_file.Get(tagger)
        for jetcol in [key.GetName() for key in tagdir.GetListOfKeys()]:
            # only dump efficiencies for selected jet collections (in config.yaml)
            if jetcol not in selected_jet_collections:
                continue
            jetcoldir = tagdir.Get(jetcol)
            result[tagger][jetcol] = {}
            for wp in [key.GetName() for key in jetcoldir.GetListOfKeys()]:
                if "FixedCut" not in wp:
                    continue
                wpdir = jetcoldir.Get(wp)
                result[tagger][jetcol][wp] = {}
                # look up MC IDs with B flavour as dummy
                mc_ids = set()
                for objectlist in wpdir.Get("B").GetListOfKeys():
                    if "Eff" in objectlist.GetName():
                        # assume name scheme like 410250_Eff
                        mc_ids.add(str(objectlist.GetName()).split("_")[0])
                for mc_id in mc_ids:
                    result[tagger][jetcol][wp][mc_id] = {}
                    print(tagger, jetcol, wp, mc_id)
                    # order of Light, C, B, T is hard-coded in btagtools
                    effs = getbtaginfo(cdi_path, tagger, wp, jetcol, mc_id, pt_low, pt_high)
                    result[tagger][jetcol][wp][mc_id]["Light"] = {}
                    result[tagger][jetcol][wp][mc_id]["Light"][f"pt_{pt_low}"] = round(effs[0], 4)
                    result[tagger][jetcol][wp][mc_id]["Light"][f"pt_{pt_high}"] = round(effs[1], 4)
                    result[tagger][jetcol][wp][mc_id]["C"] = {}
                    result[tagger][jetcol][wp][mc_id]["C"][f"pt_{pt_low}"] = round(effs[2], 4)
                    result[tagger][jetcol][wp][mc_id]["C"][f"pt_{pt_high}"] = round(effs[3], 4)
                    result[tagger][jetcol][wp][mc_id]["B"] = {}
                    result[tagger][jetcol][wp][mc_id]["B"][f"pt_{pt_low}"] = round(effs[4], 4)
                    result[tagger][jetcol][wp][mc_id]["B"][f"pt_{pt_high}"] = round(effs[5], 4)
                    result[tagger][jetcol][wp][mc_id]["T"] = {}
                    result[tagger][jetcol][wp][mc_id]["T"][f"pt_{pt_low}"] = round(effs[6], 4)
                    result[tagger][jetcol][wp][mc_id]["T"][f"pt_{pt_high}"] = round(effs[7], 4)
    return result


def main():
    with open(join("data", "config.yaml")) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)

    # dump flavour tagging efficiencies
    data = {}
    cdi_basepath = config["cdi_basepath"]
    for cdi in config["cdi_files"]:
        cdi_path = join(cdi_basepath, cdi)
        data[cdi] = {}
        data[cdi]["efficiencies"] = dumpEfficiencies(cdi_path, config)
        data[cdi]["cdi_path"] = cdi_path

    makedirs(join("output", "yaml"), exist_ok=True)
    with open(join("output", "yaml", "cdi_efficiencies.yaml"), "w") as file:
        yaml.dump(data, file, default_flow_style=False)


if __name__ == "__main__":
    main()
