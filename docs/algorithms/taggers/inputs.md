# Inputs to Flavor Tagging

## Jets

Flavor tagging runs on a number of jet collections. See the [[JetEtMiss]] twikis for a lot more info.

The steps to tagging a jet are:

- Define a jet collection. There are currently a few standard jet collections for [small-R][smallr] and [big-R][bigr] jets.
- Associate tracks to jets. See the discussion below.
- Apply some track selection. There are a number of track selection options hard coded in Athena, for example [`R22_DEFAULT`][r22def].
- Feed the selected tracks to a downstream neural network.

### Association Radii

The various radii relevant for flavor tagging are summarized in the figure below [[source](https://github.com/dguest/vr-jet-corner-cases)].

![radii](../../assets/radii-vs-pt.svg)

Notes:

- The (cone-based) truth labeling use a fixed radius, $\Delta R < 0.3$. If a $b$-hadron is found within this cone the jet is labeled as a $b$-jet, otherwise the procedure is repeated for $c$ hadrons, and then $\tau$ leptons, until a match is found..
- Both the track association and the labelling cone assign objects to the _closest_ jet. The algorithm is thus very sensitive to lower pt threshold for jets to be reconstructed.
- Historically small radius jets used a shrinking cone for this association, defined by $\Delta R < 0.239 + \exp(-1.22 - 16.4/\text{TeV} \cdot p_{\rm T})$. Large radius jets rely on ghost association.

[smallr]: https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Reconstruction/Jet/JetRecConfig/python/StandardSmallRJets.py
[bigr]: https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Reconstruction/Jet/JetRecConfig/python/StandardLargeRJets.py
[r22def]: https://gitlab.cern.ch/search?search=TrackSelection%3A%3AR22_DEFAULT&project_id=53790&group_id=4114&search_code=true&repository_ref=main




## Tracks

The tracks which are used in the flavour tagging algorithms are selected according to the recommendations of the **tracking combined performance group**.

The tracks are reconstructed from hits in the inner detector, which is described in greater detail [in the Tracking CP tutorial](https://atlassoftwaredocs.web.cern.ch/trackingTutorial/idoverview/).
The resulting reconstructed tracks are provided via the main track container *InDetTrackParticles*. It contains merged tracks from several track reconstruction passes:

- silicon-seeded
- [backtracking](https://atlassoftwaredocs.web.cern.ch/trackingTutorial/trtseeded/)
- TRT-only

Link to [track selection criteria on tracking CP TWiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Selection_Criteria)


The recommended track selections in r22 for EMPFlow jets in the [`training-dataset-dumper`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) are:

- [`r22default-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/r22default-track-cuts.json) (replacing `ip3d-loose-track-cuts.json`)
- [`r22loose-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/r22loose-track-cuts.json) (replacing `gnn-track-cuts.json`)



???+ bug "Change in minimum hit multiplicity requirement for silicon-seeded tracks in r22"



    For rel22 optimisation, the minimum number of silicon hits for silicon-seeded tracks was increased from 7 to 8 (now requiring 8 or more hits).
    More information is provided in these [slides from 31.01.2018](https://indico.cern.ch/event/690163/contributions/2856996/attachments/1592580/2520965/7to8hits_PP.pdf#search=jansky%20selection).
    The requirement for [backtracking tracks](https://atlassoftwaredocs.web.cern.ch/trackingTutorial/trtseeded/) is not changed and still requires more than 4 silicon hits.

    There was an oversight: the “Loose” track definition should require at least 8 silicon hits but the [InDetTrackSelectionTool](https://gitlab.cern.ch/atlas/athena/-/blob/master/InnerDetector/InDetRecTools/InDetTrackSelectionTool/Root/InDetTrackSelectionTool.cxx) required at least 7. This has been fixed in a [merge request](https://gitlab.cern.ch/atlas/athena/-/merge_requests/57448/), the `master` branch of Athena is fixed as of Oct 18.

    The effect of this oversight was that backtracking-based tracks with exactly 7 Si hits pass the selection when they should not.
    Only "Loose" tracks have been affected, the "TightPrimary" tracks are not affected. Track reconstruction in general also is not affected. : the track containers created in reconstruction remain the same. Only the track selection tool is required to be updated (equivalently the FTAG track selection in the training-dataset-dumper needs an update). Also vertexing (which is based on TightPrimary tracks) is not affected.

    The [`training-dataset-dumper`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) configurations

    - [`ip3d-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/ip3d-track-cuts.json)
    - [`ip3d-loose-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/ip3d-loose-track-cuts.json)
    - [`gnn-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/gnn-track-cuts.json)

    are based on the old (non-recommended) track selection criteria.

    The configurations 

    - [`r22default-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/r22default-track-cuts.json) (replacing `ip3d-loose-track-cuts.json`)
    - [`r22loose-track-cuts.json`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/fragments/r22loose-track-cuts.json) (replacing `gnn-track-cuts.json`)

    account for the change in minimum hit multiplicity requirement for silicon-seeded tracks in r22.

    Related merge requests and issues:

    - [`atlas-flavor-tagging-tools/training-dataset-dumpertraining-dataset-dumper` Issue 96](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/issues/96)
    - [`atlas-flavor-tagging-tools/training-dataset-dumpertraining-dataset-dumper` MR 427](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/427)
    - [`atlas/athena` MR 58144](https://gitlab.cern.ch/atlas/athena/-/merge_requests/58144)
