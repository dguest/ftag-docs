# FTAG Algorithms Documentation

## General Information

### Conveners
- [Samuel Van Stroud](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=13799)
- [Nicole Hartman](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=10001)

to be reached via [atlas-ftag-algorithms-conveners@cern.ch](mailto:atlas-ftag-algorithms-conveners@cern.ch)


### E-groups & Mailing List
The group mailing list is:
[atlas-cp-flavtag-btagging-algorithms@cern.ch](mailto:atlas-cp-flavtag-btagging-algorithms@cern.ch)

If you want to follow group activities, and access certain resources, it is highly recommended to 
subscribe to the following egroups using the [CERN egroups webpage](https://e-groups.cern.ch/e-groups/EgroupsSearch.do).

- `atlas-cp-flavtag-btagging-algorithms`
- `atlas-cp-flavtag-jetetmiss-BoostedXbbTagging`


### Meetings
The meetings are taking place every Thursday at 13h00 CERN time: [https://indico.cern.ch/category/9120/](https://indico.cern.ch/category/9120/).

A meeting overview is available [here](../meetings/algorithms.md).


### Mattermost
You can join our Mattermost channel [here](https://mattermost.web.cern.ch/signup_user_complete/?id=1wicts5csjd49kg7uymwwt9aho&md=link&sbr=su).

It is used to discuss a variety of topics and is the easiest way to ask questions.


### Positions in algorithms group

- Sample manager: [Neelam Kumari (DESY)](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=13393)
- Salt framework manager: [Nikita Pond (UCL)](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=16390)
- Puma software manager: [Dmitrii Kobylianskii (Weizmann)](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=16667)


