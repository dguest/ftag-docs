# Trackless-b-tagging activities

## Introduction

Track-less b-tagging uses information about hits (or their absence) on detector layers to infer the presence of b-jets.

## Reconstruction tags 

We have used the following r-tags for AODs:

- [`r13258`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r13258): saves hits with dR(hit,jet)<0.4 and pt(jet)>300 GeV
- [`r14736`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r14736): saves hits with dR(hit,jet)<0.4 and pt(jet)>20 GeV

## Recommended samples for trackless-b-tagging studies

| name        | h5     | DxAOD  | AOD      | comments |
| ----------- | ------ | ------ |--------- | -------- |
| extended Z' | `user.rkusters:user.rkusters.800030.e7954_s3778_r13258_r13146_p5906.tdd.TracklessEMPFlow.24_2_25.23-10-21_trackless_p5906_output.h5` | `mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3778_r13258_p5906` | `mc16_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e7954_s3778_r13258` | sample is `mc20d` for r22 studies even though it is labelled `mc16`. Hits in h5 files are saved in a cone of dR=0.1 around the jet axis. |
| ttbar | `ser.rkusters:user.rkusters.410470.e6337_s3681_r14736_r14672_p5906.tdd.TracklessEMPFlow.24_2_25.23-10-21_trackless_p5906_output.h5` | `mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3681_r14736_p5906`| `mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r14736` | Hits in h5 files are saved in a cone of dR=0.1 around the jet axis. |

You can download the datasets from the grid using `rucio`: [ATLAS software tutorial about grid downloads with `rucio`](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/rucio_download_files/).

