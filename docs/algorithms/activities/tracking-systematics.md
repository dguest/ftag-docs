# Effects of tracking systematics on algorithm performance

This study aims to quantify the impact of detector material variations in the ATLAS detector on flavor tagging performance. We achieve this by examining flavor tagging efficiencies in Monte Carlo samples simulated with additional detector material. Results are presented for 𝑏-jets, 𝑐-jets and light-jets. An attempt is made at developing a procedure to reweight our nominal sample based on the number of tracks from material interactions to match DL1 and DL1r distributions in alternative samples. Overall, we find that uncertainties from material variations can be sufficiently quantified via existing tools.

A full summary of this study can be found here: 

- [https://cds.cern.ch/record/2825844](https://cds.cern.ch/record/2825844)

with all code stored here:

- [https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/qualification-tasks/tracking-uncertainties-study](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/qualification-tasks/tracking-uncertainties-study)
