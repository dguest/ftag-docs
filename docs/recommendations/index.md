# Flavor Tagging Recommendations

As a physics analyst you might be interested in two things from the flavor tagging group:

1. [Expected algorithm performance in MC][expected-algorithm-performance].
2. [data/MC scale factors for analyses][ftag-calibration-recommendations].
