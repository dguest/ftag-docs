# FTAG Algorithms In r22

The current efforts in the algorithm group are mainly concentrating on release 22 developments.

For r22 analysis, two series of algorithms have been trained and released.

- The DL1d series is using the same two-tiered strategy which has been used in r21 for the late-Run-2 algorithms. Low-level taggers based on expert knowledge and specialised task provide input to a high-level feed-forward neural network. For the DL1d algorithm, the [RNNIP algorithm](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-003/) has been replaced by [DIPS][dips-tagger] and consequently use DIPS as input for the [DL1 algorithm][dl1-tagger].
- The [GN1][gn1-tagger] / [GN2][gn2-tagger] series emply a single architecture, directly operating on track inputs and jet inputs. Auxiliary tasks for track origin prediction and track vertex finding improve the performance. GN1 and GN2 differ in architecture and hyperparameters, as well in size of the training dataset.


## Preliminary recommendation as of 07.10.2023

One version of GN2 exists to date whic his recommended for studies. The currently recommended version has the name `GN2v00`. Because of technical limitations of the Athena b-tagging selection tool, there are two sets of operating points which are released to offer more than just the conventional four operating points for analysis teams to test.

The GN1 tagger is only released as an experimental time-stamped version which is not supported.

The DL1d tagger is released as time-stamped versions exist to date. The currently recommended version has the name `DL1dv01`. The network definition is located in `/eos/atlas/atlascerngroupdisk/asg-calib/BTagging/20220509/dl1dLoose/antikt4empflow/network.json` or [via the calibration area web interface](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20220509/dl1dLoose/antikt4empflow/network.json). 
Note that it relies on [the dips training `dipsLoose20220314v2`](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20220314/dipsLoose/antikt4empflow/network.json), which must be scheduled before `DL1dv01`.

### Working point definition for GN2v00

Discriminant defined with $f_c=0.1$.

Because the `GN2v00` algorithm provides an unprecedented discrimination power among b-jets, c-jets and light-flavour jets, it should be investigated in detail by analysis teams.
To facilitate analyses testing different GN2 OPs, allowing analyses to achieve this goal with minimal technical hurdles, two tagger names are registered in the software. They are effectively the same tagger but the OPs are defined differently:

- `GN2v00LegacyWP`: This set maintains the traditional b-tag efficiency levels of 60%, 70%, 77%, and 85%.  PCBT is defined using the same OPs. 
- `GN2v00NewAliasWP`: The OPs correspond to b-tag efficiencies of 68%, 76%, 82%, and 89%. They were derived by fixing the light-flavour jet mis-tagging efficiency to the level achieved with DL1r in rel21. PCBT is defined using the same OPs. 

Both taggers have their `fixCut` folders in the CDI file named according to the traditional convention of 60%, 70%, 77%, and 85%. However, for `GN2v00NewAliasWP`, these correspond to **68%, 76%, 82%, and 89%**, respectively.

This means an analysis only needs to register those new tagger names in the analysis framework, and does not have to add new OPs since their names remain the same. 


| Tagger name in SW | OP name         | Efficiency | Discriminant cut |
| ----------------- | --------------- | ---------- | ---------------- |
| GN2v00LegacyWP    | FixedCutBEff_60 | 60%        | 5.394            |
| GN2v00NewAliasWP  | FixedCutBEff_60 | 68%        | 4.157            |
| GN2v00LegacyWP    | FixedCutBEff_70 | 70%        | 3.875            |
| GN2v00NewAliasWP  | FixedCutBEff_70 | 76%        | 3.034            |
| GN2v00LegacyWP    | FixedCutBEff_77 | 77%        | 2.893            |
| GN2v00NewAliasWP  | FixedCutBEff_77 | 82%        | 2.145            |
| GN2v00LegacyWP    | FixedCutBEff_85 | 85%        | 1.638            |
| GN2v00NewAliasWP  | FixedCutBEff_85 | 89%        | 0.783            |


Please do refer to the TWiki [BTagRel22HighLevelSummary](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagRel22HighLevelSummary) for further details.


### Algorithms Expected Performances in MC for GN2v00

A summary of the expected performance of `GN2v00` on PFlow jets for the different available operating points can be found in the following plots. For comparison, the performance of the `DL1dv01` and `GN120220509` taggers are overlaid.

All plots are only for internal use and not to be shown outside of the collaboration.

In addition, the c-jet and light-flavour jet rejection for the eight defined efficiency operating points is evaluated in jets from simulated ttbar events ($p_{T}$ > 20 GeV, $|\eta|$ < 2.5).

| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | c-rejection | light-flavour rejection |
| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- |
| GN2v00 | FixedCutBEff_60 | 5.394 | 0.60 | 0.00436 | 0.00011 | 229.36 | 9473.81 |
| GN2v00 | FixedCutBEff_68 | 4.157 | 0.68 | 0.01652 | 0.00037 | 60.54 | 2689.21 |
| GN2v00 | FixedCutBEff_70 | 3.875 | 0.70 | 0.02281 | 0.00053 | 43.84 | 1902.75 |
| GN2v00 | FixedCutBEff_76 | 3.034 | 0.76 | 0.05763 | 0.00152 | 17.35 | 658.83 |
| GN2v00 | FixedCutBEff_77 | 2.893 | 0.77 | 0.06672 | 0.00183 | 14.99 | 547.03 |
| GN2v00 | FixedCutBEff_82 | 2.145 | 0.82 | 0.13619 | 0.00479 | 7.34 | 208.65 |
| GN2v00 | FixedCutBEff_85 | 1.638 | 0.85 | 0.20418 | 0.00893 | 4.90 | 111.96 |
| GN2v00 | FixedCutBEff_89 | 0.783 | 0.89 | 0.33801 | 0.02338 | 2.96 | 42.76 |



- INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_roc_roc_GN2v00_b.png)

??? example "GN2v00LegacyWP: FixedCutBEff_60 (60% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_60 (68% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 68% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 68% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_68.png)

    - INTERNAL light jet rejection as a function of jet pT for 68% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 68% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_68.png)

    - INTERNAL c jet rejection as a function of jet pT for 68% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 68% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_68.png)

??? example "GN2v00LegacyWP: FixedCutBEff_70 (70% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_70 (76% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 76% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 76% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_76.png)

    - INTERNAL light jet rejection as a function of jet pT for 76% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 76% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_76.png)

    - INTERNAL c jet rejection as a function of jet pT for 76% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 76% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_76.png)

??? example "GN2v00LegacyWP: FixedCutBEff_77 (77% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_77 (82% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 82% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 82% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_82.png)

    - INTERNAL light jet rejection as a function of jet pT for 82% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 82% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_82.png)

    - INTERNAL c jet rejection as a function of jet pT for 82% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 82% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_82.png)

??? example "GN2v00LegacyWP: FixedCutBEff_85 (85% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_85 (89% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 89% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 89% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_89.png)

    - INTERNAL light jet rejection as a function of jet pT for 89% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 89% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_89.png)

    - INTERNAL c jet rejection as a function of jet pT for 89% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 89% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_89.png)


### Working point definition for DL1dv01

Discriminant defined with $f_c=0.018$.

| Efficiency | Discriminant cut |
| ---------- | ---------------- |
| 60%        | 4.854            |
| 70%        | 3.493            |
| 77%        | 2.456            |
| 85%        | 0.948            |

### Algorithms Expected Performances in MC for DL1dv01

A summary of the expected performance of `DL1dv01` on PFlow jets for the different available working points can be found in the following plots. For comparison, the performance of the `DL1dv00` and `DL1r` (retrained in r22 for comparison) taggers are overlaid.

All plots are only for internal use and not to be shown outside of the collaboration.

In addition, the c-jet and light-flavour jet rejection for the four defined efficiency operating points is evaluated in jets from simulated ttbar events ($p_{T}$ > 20 GeV, $|\eta|$ < 2.5).

| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | c-rejection | light-flavour rejection |
| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- |
| DL1dv01 | FixedCutBEff_60 | 4.854 | 0.60 | 0.01902 | 0.00024 | 52.57 | 4175.17 |
| DL1dv01 | FixedCutBEff_70 | 3.493 | 0.70 | 0.06937 | 0.00113 | 14.41 | 883.32 |
| DL1dv01 | FixedCutBEff_77 | 2.456 | 0.77 | 0.15542 | 0.00380 | 6.43 | 263.50 |
| DL1dv01 | FixedCutBEff_85 | 0.948 | 0.85 | 0.32864 | 0.01916 | 3.04 | 52.20 |


- INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_roc_roc_DL1dv01_b.png)

??? example "FixedCutBEff_60 (60% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

??? example "FixedCutBEff_70 (70% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

??? example "FixedCutBEff_77 (77% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

??? example "FixedCutBEff_85 (85% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)


## Preliminary recommendation as of 27.06.2022 (superseded)

??? example "Working point definition for DL1dv01"

    Discriminant defined with $f_c=0.018$.

    | Efficiency | Discriminant cut |
    | ---------- | ---------------- |
    | 60%        | 4.854            |
    | 70%        | 3.493            |
    | 77%        | 2.456            |
    | 85%        | 0.948            |

??? example "Algorithms Expected Performances in MC for DL1dv01"

    A summary of the expected performance of `DL1dv01` on PFlow jets for the different available working points can be found in the following plots. For comparison, the performance of the `DL1dv00` and `DL1r` tagger (r21 recommendation - i.e. trained in r21) is overlaid.

    All plots are only for internal use and not to be shown outside of the collaboration.

    - INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

    ![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-preliminary-recommendations/roc_DL1dv01_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_60_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_60_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_60_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_70_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_70_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_70_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_77_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_77_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_77_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_85_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_85_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_85_ttbar.png)


## Experimental recommendation as of 27.06.2022  (superseded)

The GN1 tagger (timestamp: `GN120220509`) is released in derivations for experimental testing of analysis teams. The network definition is located in `/eos/atlas/atlascerngroupdisk/asg-calib/BTagging/20220509/gn1/antikt4empflow/network.onnx` or [via the calibration area web interface](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20220509/gn1/antikt4empflow/network.onnx).

The GN1 tagger is superseded by the GN2 tagger with version `GN2v00`.

??? example "Working point definition for GN120220509"

    Discriminant defined with $f_c=0.05$.

    | Efficiency | Discriminant cut |
    | ---------- | ---------------- |
    | 60%        | 5.135            |
    | 70%        | 3.642            |
    | 77%        | 2.602            |
    | 85%        | 1.253            |

??? example "Algorithms Expected Performances in MC for GN120220509"

    A summary of the expected performance of `GN120220509` on PFlow jets for the different available working points can be found in the following plots. For comparison, the performance of the `DL1dv01` and `DL1r` tagger (r21 recommendation - i.e. trained in r21) is overlaid.

    All plots are only for internal use and not to be shown outside of the collaboration.

    - INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

    ![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-preliminary-recommendations/roc_GN120220509_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_60_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_60_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_60_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_70_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_70_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_70_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_77_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_77_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_77_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_85_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_85_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_85_ttbar.png)


## Preliminary recommendation as of 08.10.2021 (superseded)


The name of this tagger is `DL1dv00` and the network definition is located in `/eos/atlas/atlascerngroupdisk/asg-calib/BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json` or [via the calibration area web interface](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json). 
Note that it relies on [the dips training `dipsLoose20210729`](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20210729/dipsLoose/antikt4empflow/network.json), which must be scheduled before `DL1dv00`.

The DL1d tagger with version `DL1dv00` is superseded by the DL1d tagger with version `DL1dv01`.


??? example "Working point definition for DL1dv00"

    Discriminant defined with $f_c=0.018$.

    | Efficiency | Discriminant cut |
    | ---------- | ---------------- |
    | 60%        | 4.884            |
    | 70%        | 3.494            |
    | 77%        | 2.443            |
    | 85%        | 0.930            |

??? example "Algorithms Expected Performances in MC for DL1dv00"

    A summary of the expected performance of `DL1dv00` (preliminary recommendation- trained on r22) on PFlow jets for the different available working points can be found in the following plots.
    For comparison, the performance of the `DL1r` tagger (r21 recommendation - i.e. trained in r21) is overlaid.

    All plots are only for internal use and not to be shown outside of the collaboration.

    - INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

    ![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-preliminary-recommendations/DL1dloosev00_DL1r_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_60eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_60eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_60eff_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_70eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_70eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_70eff_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_77eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_77eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_77eff_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_85eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_85eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_85eff_ttbar_r22_0.png)
