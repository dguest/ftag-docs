# Open tasks

Here you can find a list of tasks looking for owner.
If you want to adopt a task or know a PhD student looking for 
a AQP project, you are in the right place!


??? example "Scale Factors Smoothing"

    The responsible person will take care of re-optimising the SFs smoothing for Run3 data and implement a MC-to-MC smoothing in the main CDI file

??? example "Combination of calibration"

    The responsible person will take care combinaning the SFs produced by difference calibrations when more than one is available for a given jet flavour.

??? example "Calibration of light jets with multijet events"

    The responsible person will take care of migrating the multi-jet events light jets calibration from R21 to R24. They will derive Run2+Run3 SFs for the GN2 tagger and compare to other existing results. The final part of the task consists in documenting the code.

??? example "Calibration of charm-jets with ttbar single lepton events"

    The responsible person will take care of migrating the ntuple production code with another code to account for AnalysisTop being phased-out. They will perform closure tests comparing results with the new optimised framework and the old one in Run3 data. They will produce data/mc comparison plots, compare the selection efficiencies, the impact of uncertainties and finally the data-to-MC scale factors. The final part of the task consists in documenting the code.

