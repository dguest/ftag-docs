

# Pseudo-continuous b-tagging


The goal of a heavy flavour tagger calibration is to reweight the tagger-score distribution for all observable jet types in a MC sample (b-, c-, and light-jets) such that it more closely matches with the tagger-score distribution for the corresponding jet types in real data. 

This is difficult to do *directly* on the tagger-score distribution, which is a continuous distribution (though there are efforts to perform such a calibration through [optimal transport](https://arxiv.org/abs/2106.01963)), but you *can* re-weight - and thus calibrate - the *binned* tagger-score distribution. The fixed-cut working points already sub-divide the tagger-score into parts (tagged vs. not-tagged). We can repurpose these cuts on the tagger-score as the bin edges of the so-called **tag-weight bins**. Given $n$ efficiency working-points of a b-jet tagger, you can define $n-1$ tag-weight bins which contain the fraction of events with tagger discriminants contained in those tag-weight bins.

Taking the current DL1r calibration working-points as an example, the DL1r score is split into intervals of $[100\%, 85\%, 77\%, 70\%, 60\%, 0\%]$ , which includes the trivial $100\%$ and $0\%$ working-points that either accept *everything* as a b-jet, or *nothing* as a b-jet, respectively. In this way, every jet is "b-tagged" by the fact that the DL1r score must fall into some tag-weight bin, whether it falls into the "loosest" bin (from the $100\%$-$85\%$ working points) or in the "tightest" bin (from the $60\%$-$0\%$ working points).


The method of **pseudo-continuous b-tagging (PCBT)** yields a normalized tag-weight distribution for every kinematic bin of the fixed-cut calibrations. In other words, the PCBT efficiency bins are the **conditional probability $P_{b}(O^{k} | T^{m})$ for a b-jet of kinematic bin $(T^{m})_{1...9}$ to have a tagger-score falling in the efficiency range $(O^{k})_{k=1...5}$**.  

Hence, no matter the jet kinematics which typically determine whether or not a jet is tagged under a fixed-cut scheme, the same jet will *always* be tagged at a looser working point in PCBT, going down to the loosest tag-weight bin. You can utilize this information - that is, the information provided by the *entire* tag-weight calibration - to define various signal and control regions at the analysis level.

## Applying calibrated flavour tagging to analysis


Below you can see an example PCBT tag-weight distribution (left) for a particular $p_T$ bin. At the analysis level, you can utilize the tag-weight distribution, for example, at the **exclusive** working-point of $70-60\%$. What this *means* is that you will tag a jet if it's tagger-score falls in that particular PCBT bin. In the fixed-cut working-point, one would use an **inclusive** working-point of $70\%$, which would also tag a jet if its' score falls in the $60-0\%$ bin. **Note: you should be able to reconstruct the inclusive efficiency for a particular fixed-cut working-point by summing over the PCBT efficiency bins that are below that working-point.**

Similar to the fixed-cut scheme, the calibration of the continuous working point yields **efficiency scale-factors** which reweight the tagged jets in your MC sample, and **(in)efficiency scale-factors** for the mis-tagged jets. As you can see in the plot below (right), these scale-factors are also defined per tag-weight bin, per kinematic bin.


![Tag Weight Probability](../../assets/cdi/tagweightscores.png)


Whenever a jet is b-tagged at some exclusive working-point (its tagger-score falls within the b-jet PCBT bin you care about), then you would apply the corresponding efficiency scale-factor for a b-jet, like so:

$$
w_{jet} = SF_{b}(T^{m}, O^{i}) = \frac{P^{data}_{b}(O^{i} | T^{m} ) }{P^{MC}_{b}(O^{i} | T^{m} )}
$$


As the PCBT bins are directly related to the fixed-cut working-points, you can derive the PCBT efficiency scale-factors for the $i^{th}$ PCBT bin by taking the ratio of their difference

$$
SF_{b}(T^{m}, O^{i}) = \frac{\epsilon^{MC}_{i} (T^{m}) SF_{i} (T^{m}) - \epsilon^{MC}_{i+1} (T^{m}) SF_{i+1} (T^{m})  }{\epsilon^{MC}_{i} (T^{m}) - \epsilon^{MC}_{i+1} (T^{m})}
$$

where all of the RHS corresponds to the efficiencies and scale-factors of b-jets, and $T^{m}$ is the $m^{th}$ kinematic bin in $p_T$, such that in this context $SF(T^{m})$ would refer to the b-jet scale-factor for that kinematic bin. **Note: The above is just illustrative of the relationship between PCBT and fixed-cut. As the calibrations are performed over the entire PCBT distribution, the PCBT efficiency scale-factors are already accessible from the calibration itself, and this conversion is not needed in practice.**

To handle any *mis-tagged jets*, in the PCBT method you would simply weight the jet with the corresponding flavour (in)efficiency scale-factor:

$$
w_{jet} = SF_{c/l}(T^{m}, O^{i}) = \frac{P^{data}_{c/l}(O^{i} | T^{m} ) }{P^{MC}_{c/l}(O^{i} | T^{m} )}
$$

