# CDI file structure

The CDI files internally map (via ROOT `TMap`'s) a flavour tagging algorithm's performance on a particular **jet collection**, **fixed cut working-point (WP)**  - sometimes called the operating point (OP) - and the **jet flavour**. You can find the recommended CDI files at `cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/` in LXPLUS, and you can interact with them *almost* the same as other regular ROOT files.

I say "almost" because, while these ROOT files do essentially store ROOT `TH1` histograms of tagger efficiencies with respect to jet kinematics, since these are within ROOT `TMap`'s, you cannot simply plot or draw these histograms. For that, there are some tools available at [`validation_CDI`](https://gitlab.cern.ch/atlas-ftag-calibration/validation_cdi) that can do this with relative ease.

> *Note: a webpage with automated CDI files plots is in the works. This page will have all relevant plots from the CDI files, such as efficiency scale-factor plots and efficiency maps for all available generators, working-points, jet collections, and flavour-taggers.*

##### Internal structure of a CDI file

There are a number of ways to access the contents of CDI files. One of the most straightforward methods would be via the ROOT **TBrowser**, in which you can access the internal directory structure of a CDI file directly.


![CDI TBrowser](../../../assets/cdi/inspect-cdi.png)


The only jet flavour discimination power we currently have is to tell apart so-called heavy flavour jets (b- or c-jets) from light jets, i.e. those initiated from the u-, d-, or s-quarks. In addition, there is some ability to tag tau leptons, bringing the total list of experimentally observable flavours to four in total: **B**, **C**, **Light**, and **T**.

These flavours each come with associated uncertainties which are stored as **systematic variations** within the CDI files. Each flavour has roughly 50 such variations, stored alongside the central calibration and other ROOT **TH1** objects.




