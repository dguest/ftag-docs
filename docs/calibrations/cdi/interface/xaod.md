# xAOD B-Tagging Interface


The **CalibrationDataInterface** provides a fairly low-level interface in the form of the `CalibrationDataInterfaceROOT` class. This is the primary way by which an analysis can access flavour tagging calibration information directly from the CDI files. 

This is usable on its own, but it is largely more convenient to use the **xAODBTaggingEfficiency** package, which has pre-made wrapper classes (tools) that enable CDI access in an easy-to-use way. Like any other tool, these tools each come in handy in some common flavour-tagging use-cases for which they were specifically designed to be used. Below you can find a brief listing of what each tool is used for, which is further expounded upon in later sections of this document.


| B-tagging Tool | Description and Use-Cases |
| :-------------------- | ---------------- |
| **BTaggingEfficiencyTool**        | This tool enables analysers to access flavour-tagging calibration scale-factors. By setting up this tool with a combination of a CDI file location (where calibrations are stored), for a given tagging algorithm, jet-collection, and working point, you can very simply apply scale-factors directly to your sample event's `xAOD::Jet` objects. This is very commonly used at the analysis level to apply the flavour tagging calibration (scale-factor re-weighting) on your MC samples.           |
| **BTaggingSelectionTool**      | Similar to the `BTaggingEfficiencyTool`, this interface allows analysers to access the flavour tagging information on an `xAOD::Jet` object. However, this tool merely returns information about the tagged/not-tagged status of your jet, given a tagging algorithm, jet-collection, and working-point. No scale-factors are applied in this simple tool, and as such it is normally used for studies of the b-tagging content of your samples, or whenever calibration information *isn't available* for a tagger you wish to use.   |
| **BTaggingTruthTaggingTool**   |   This tool is used when applying the so-called **truth-tag method**, wherein one re-weighs events based on their *probability* to pass a flavour-tagging required (like containing a certain number of c-jets), rather than re-weighting by the scale-factor of the selection requirement directly. This is useful in analyses where the tagging efficiency of your tagging algorithm is so low as to be prohibitive, and would negatively affect your event selection, as for an analysis using c-tagging which currently has a very low tagging efficiency. **Note: this tool uses the `BTaggingEfficiencyTool` internally to access systematic uncertainty variations. Read more on this in the following sections of this document.**|
| **BTaggingEigenVectorRecompositionTool**   |  This unique tool specifically applies to cases wherein analyses makes use of the eigenvector decomposition systematic strategies of the `BTaggingEffiencyTool` to handle systematic uncertainty variations in their analysis (which is the recommendation to reduce the number of variations needed). The tool itself attempts to compute the contribution of each original systematic uncertainty (pre-PCA) to every eigenvector variation constructed from the PCA, thus essentially *reconstructing the original set of systematics that contribute to your eigenvector variation*. This is useful when attempting to correlate the systematics across analyses, as in general the eigenvector variations constructed for different taggers and working-points *are distinct from one another*.  |

## Using the `xAODBTaggingEfficiency` package in an analysis

In order to make use of *any* of the wrapper classes found in the `xAODBTaggingEfficiency` package in an analysis, you need to `#include` the tool interfaces (typically in your header file), and add the relevant libraries to your `CMakeLists.txt` file, like so:

```
atlas_add_library (
    ...
    LINK_LIBRARIES
        ...
        FTagAnalysisInterfacesLib
        ...
)
```

where the `FTagAnalysisInterfaces` contains the interfaces of all the tools. 

### Setting up the `BTaggingEfficiencyTool`

In your header file:

``` c++
#include <AsgTools/AnaToolHandle.h>
#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>
#include <xAODBTaggingEfficiency/BTaggingEfficiencyTool.h> // direct include
```

wherein you can directly point to the `BTaggingEfficiencyTool` class. If you opt to `#include` this directly, then in your `CMakeLists.txt` add `xAODBTaggingEfficiencyLib` to your `LINK_LIBRARIES` like so:

```
atlas_add_library (
    ...
    LINK_LIBRARIES
        ...
        FTagAnalysisInterfacesLib
        xAODBTaggingEfficiencyLib
        ...
)
```



### Setting up the `BTaggingSelectionTool`

Similarly, in your header file:

``` c++
#include <AsgTools/AnaToolHandle.h>
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <xAODBTaggingEfficiency/BTaggingSelectionTool.h> // add library to CMakeLists.txt
```

### Setting up the `BTaggingTruthTaggingTool`

In your header file:

``` c++
#include <AsgTools/AnaToolHandle.h>
#include <FTagAnalysisInterfaces/IBTaggingTruthTaggingTool.h>
#include <xAODBTaggingEfficiency/BTaggingTruthTaggingTool.h> // add library to CMakeLists.txt
```


### Setting up the `BTaggingEigenVectorRecompositionTool`

In your header file:

``` c++
#include <AsgTools/AnaToolHandle.h>
#include <FTagAnalysisInterfaces/IBTaggingEigenVectorRecompositionTool.h>
#include <xAODBTaggingEfficiency/BTaggingEigenVectorRecompositionTool.h> // add library to CMakeLists.txt
```