# GPU training resources

CERN provides a range of GPU resources which can be used for training of the flavour tagging algorithms.
This page presents a list of available resources together with instructions on their usage.

## lxplus-gpu.cern.ch

`lxplus-gpu.cern.ch` are special nodes for interactive computing provided by CERN. They behave similarly as the typical environment you are provided with when logging into `lxplus`, only that these lxplus nodes have GPU support.

To log in, enter in a terminal 

```bash
ssh -Y <username>@lxplus-gpu.cern.ch
````

Once you have logged in, you can inspect the avaliable GPU resources with

```bash
nvidia-smi
```

## SWAN

SWAN (an acronym for service for web-based analysis) is CERN's [jupyter-notebook](https://jupyter.org) interface. It is a web-based service that offers notebooks merging  code, text, and other media materials for interactive analysis. SWAN integrates with CERN’s infrastructure, such as users’ synchronized storage (CERNBox), computing resources, and experiments data and software.
One can request GPU support (now in beta version and limited to a small scale).


- [https://swan-k8s.cern.ch/](https://swan-k8s.cern.ch/) (Website for SWAN with Kubernetes support)

### Setup

Currently (May 2022), the CUDA-enabled software stack with GPU support is only provided for users with GPU privileges.
One can request being added to the list of GPU users by [opening a ticket with this link](https://cern.service-now.com/service-portal?id=functional_element&name=swan) (see [thread](https://swan-community.web.cern.ch/t/gpu-support-in-swan/108) for discussion). Once the ticket is approved, it is possible to log in to SWAN and select the CUDA-enabled software stack with GPU support (see screenshot below).

![SWAN configuration with GPU enabled](../assets/swan_gpu.png)

<details><summary>Note on CUDA version</summary>
When configuring the SWAN environment you will be given your choice of software stack. Be careful to use a software release with GPU support as well as an appropriate CUDA version. If you need to install additional software, it must be compatible with your chosen CUDA version.
</details>

1. After you logged in and have selected a software stack, create a new project, e.g. `ftag-algorithms`. A new project can be created by clicking the upper right "+" button.
2. Once your project has been created, you are redirected to the newly created project. You can click again on the "+" button on the upper right panel, which can now be used for creating new notebooks.
3. You can use the terminal ("`>_`" button in the upper right, one row above the "+" button) to install new packages or monitor the computing resources. Installing packages is possible with package management tools, such as `pip` for python.
	- You need to wrap the environment configuration in a environment configuration script for using installed packages. Detailed documentation on this and other features can be found by clicking the upper right "?" button.
	- Monitoring of resources is possible using the `top` / `htop` terminal applications, as well as the CUDA-specific `nvidia-smi` for listing the GPU resources.


## HTCondor with GPU resources

The lxplus batch system with the [HTCondor]() scheduler also provides the option to request GPU resources for running jobs.

- [HTCondor documentation on GPU resources](https://batchdocs.web.cern.ch/tutorial/exercise10.html)

If you are already familiar with using HTCondor, you can extend your condor submission files for using GPU resources by adding

```
request_gpus = n # n equal to the number of GPUs required (can be 0 < n < 5)
```

<details><summary>List of available GPU nodes (August 2023)</summary>
<ul>
<li> <a href="b7g57n0107.cern.ch">b7g57n0107.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n0517.cern.ch">b7g57n0517.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n0550.cern.ch">b7g57n0550.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n0636.cern.ch">b7g57n0636.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n0663.cern.ch">b7g57n0663.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n1236.cern.ch">b7g57n1236.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n1415.cern.ch">b7g57n1415.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n1508.cern.ch">b7g57n1508.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n2027.cern.ch">b7g57n2027.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n2161.cern.ch">b7g57n2161.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n2563.cern.ch">b7g57n2563.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n2819.cern.ch">b7g57n2819.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n3262.cern.ch">b7g57n3262.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n3408.cern.ch">b7g57n3408.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n3558.cern.ch">b7g57n3558.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n3982.cern.ch">b7g57n3982.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n4673.cern.ch">b7g57n4673.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n4794.cern.ch">b7g57n4794.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n4913.cern.ch">b7g57n4913.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n5101.cern.ch">b7g57n5101.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n5219.cern.ch">b7g57n5219.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n5759.cern.ch">b7g57n5759.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n5788.cern.ch">b7g57n5788.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n5816.cern.ch">b7g57n5816.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n5868.cern.ch">b7g57n5868.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n7341.cern.ch">b7g57n7341.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n7659.cern.ch">b7g57n7659.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n7802.cern.ch">b7g57n7802.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n8094.cern.ch">b7g57n8094.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n8101.cern.ch">b7g57n8101.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n8655.cern.ch">b7g57n8655.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n9049.cern.ch">b7g57n9049.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n9128.cern.ch">b7g57n9128.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n9393.cern.ch">b7g57n9393.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b7g57n9447.cern.ch">b7g57n9447.cern.ch</a> NVIDIA A100-PCIE-40GB</li>
<li> <a href="b9g47n1037.cern.ch">b9g47n1037.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n1845.cern.ch">b9g47n1845.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n1901.cern.ch">b9g47n1901.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n2030.cern.ch">b9g47n2030.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n2063.cern.ch">b9g47n2063.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n2286.cern.ch">b9g47n2286.cern.ch</a> Tesla V100-PCIE-32GB</li>
<li> <a href="b9g47n2370.cern.ch">b9g47n2370.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n2619.cern.ch">b9g47n2619.cern.ch</a> Tesla P100-SXM2-16GB</li>
<li> <a href="b9g47n2971.cern.ch">b9g47n2971.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n3052.cern.ch">b9g47n3052.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n3383.cern.ch">b9g47n3383.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n4070.cern.ch">b9g47n4070.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n4365.cern.ch">b9g47n4365.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n4710.cern.ch">b9g47n4710.cern.ch</a> Tesla V100-PCIE-32GB</li>
<li> <a href="b9g47n5201.cern.ch">b9g47n5201.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n5388.cern.ch">b9g47n5388.cern.ch</a> Tesla V100-PCIE-32GB</li>
<li> <a href="b9g47n5456.cern.ch">b9g47n5456.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n5662.cern.ch">b9g47n5662.cern.ch</a> Tesla V100-PCIE-32GB</li>
<li> <a href="b9g47n6706.cern.ch">b9g47n6706.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n7141.cern.ch">b9g47n7141.cern.ch</a> Tesla V100-PCIE-32GB</li>
<li> <a href="b9g47n7885.cern.ch">b9g47n7885.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n8171.cern.ch">b9g47n8171.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n8376.cern.ch">b9g47n8376.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n9067.cern.ch">b9g47n9067.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n9082.cern.ch">b9g47n9082.cern.ch</a> Tesla V100S-PCIE-32GB</li>
<li> <a href="b9g47n9362.cern.ch">b9g47n9362.cern.ch</a> Tesla V100-PCIE-32GB</li>
<li> <a href="b9g47n9613.cern.ch">b9g47n9613.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n9796.cern.ch">b9g47n9796.cern.ch</a> Tesla T4</li>
<li> <a href="b9g47n10645.cern.ch">b9g47n10645.cern.ch</a> Tesla V100S-PCIE-32GB</li>
</ul>
</details>