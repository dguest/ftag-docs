# Add Models to Group Area

The inference on jets for determining their jet flavour with various algorithms is performed using the trained algorithms.
These are stored either as `.json` files for evaluation with [`lwtnn`](https://github.com/lwtnn/lwtnn), the "Lightweight Trained Neural Network" implementation or as `.onnx` files for evaluation with [ONNX](https://onnx.ai), the "Open Neural Network Exchange" protocol.

The networks are scheduled using [`DL2`](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants#inputs-and-outputs-from-dl2). Using the `PathResolver`, the path of the network is searched for (in the following order) 

- in the local directory where you start the job
- `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/` (mirror of `/eos/atlas/atlascerngroupdisk/asg-calib/`)
- on https://atlas-groupdata.web.cern.ch/atlas-groupdata/ (works in docker images)


## How to add a new trained model for a flavour tagging algorithm to the group area

The trained models are hosted in the ATLAS FTAG group area.
The Twiki explaining the procedure to copy it to the group are can be found [here](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ASGCalibArea) (the copying will be done by the conveners).

Besides the `lwtnn` or `onnx` models in the group area, also the keras models together with the scale dictionaries as well as the variable dictionaries are saved in an eos area `/eos/user/u/umamibot/ftag-models` (everyone on the e-group `atlas-cp-flavtag-unami-developers` has write access).

1. Follow the [instructions for converting your model to a `lwtnn` `json` file](https://umami-docs.web.cern.ch/trainings/LWTNN-conversion/) or convert your model to an `onnx` file (whatever of the two is applicable).
2. Copy the following files to the `/eos/user/u/umamibot/ftag-models` directory (Example path: `/eos/user/u/umamibot/ftag-models/r22/<algorithm name>/<timestamp>/<jetcollection>/`)
    - keras/pytorch model files obtained from training the algorithm (but of course not the training and evaluation samples)
      - for the pytorch model, please copy both the DGL and the Vanilla pytorch model
    - converted model (`lwtnn` or `onnx`)
    - scale dictionary from umami (`scale_dict.json`)
    - variable config from umami (`variables.yaml`)


3. Ask the flavour tagging [algorithm sub-group conveners](mailto:atlas-cp-flavtag-algorithms-conveners@cern.ch) to copy the `lwtnn` or `onnx` model to the group (dev) area, by writing them the path to the file in the `/eos/user/u/umamibot/ftag-models` directory. Specify if the model should be deployed in the `dev/BTagging` (development) area for testing within the FTAG group or is ready for deployment in `BTagging/` for the whole collaboration.
4. Prepare a summary of the training, following the example below:

```yaml
# This file contains several info about an ML model deployed in FTAG
tagger_name: dipsLoose20220314v2
location_cvmfs: dev/BTagging/20220314v2/dipsLoose/antikt4empflow/network.json
umami_git_hash: 2742b4ad
gnn_jet_tagger_git_hash: Null
tdd_git_has: a9a6a393
n_jets_training: 23_000_000
training_samples:
  - user.alfroch.410470.e6337_s3681_r13144_p4931.tdd.EMPFlow.22_2_75.22-06-09-T125128_output.h5
  - user.alfroch.800030.e7954_s3681_r13144_p4931.tdd.EMPFlow.22_2_75.22-06-09-T125128_output.h5
comments:
  - upgrade model
  - using IPxD like track selection
```

The synchronisation of the group area on `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/` with the files on `/eos/atlas/atlascerngroupdisk/asg-calib/` can take a few hours.

