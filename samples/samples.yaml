ttbar:
  dsid: 410470
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, default benchmark sample for Run-2, can be used for algorithm training and performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.py
  use_for_training: ['pflow', 'vr']
  use_for_workingpoint: ['pflow', 'vr']
  use_for_mcmc: ['pflow', 'vr']

new-ttbar:
  dsid: 410470
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, new sample for Run-2, can be used for algorithm training and performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601589/mc.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.py
  use_for_training: ['pflow', 'vr']
  use_for_workingpoint: ['pflow', 'vr']
  use_for_mcmc: ['pflow', 'vr']

ttbar-af3:
  dsid: 410470
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, with ATLAS fast simulation AF3. Can be used for special performance studies of the fast simulation."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

ttbar-run3:
  dsid: [601229, 601230]
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, default benchmark sample for Run-3, can be used for algorithm training and performance studies. The Run-3 sample is split in a single-lepton+jets and a dileptonic top quark pair decay sample, which need to be stitched together. "
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601229/mc.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601230/mc.PhPy8EG_A14_ttbar_hdamp258p75_dil.py
  use_for_training: ['pflow', 'vr']
  use_for_workingpoint: ['pflow', 'vr']
  use_for_mcmc: ['pflow', 'vr']

ttbar-run3-nonallhad:
  dsid: 601589
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, default benchmark sample for Run-3. This is a special sample for FTAG, which combines the single-lepton+jets and a dileptonic top quark pair decays. It is equivalent to using the samples with DSIDs 601229 and 601230 together. The number of events is too small to be used for training. It can be used for performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601589/mc.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

ttbar-run3-highpileup:
  dsid: [601229, 601230]
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, with higher pile-up for performance studies. The Run-3 sample is split in a single-lepton+jets and a dileptonic top quark pair decay sample, which need to be stitched together."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601229/mc.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601230/mc.PhPy8EG_A14_ttbar_hdamp258p75_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

ttbar-run3-noibl:
  dsid: [601229, 601230]
  description: "Powheg+Pythia8 top quark pairs with semi-leptonic decays, with missing inner B-layer for performance studies. The Run-3 sample is split in a single-lepton+jets and a dileptonic top quark pair decay sample, which need to be stitched together."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601229/mc.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/601xxx/601230/mc.PhPy8EG_A14_ttbar_hdamp258p75_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime:
  dsid: 427080
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to b-quarks, c-quarks and light-flavour quarks, can be used for algorithm training and performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID427xxx/MC15.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.py
  use_for_training: ['vr']
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-ext:
  dsid: 800030
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to b-quarks, c-quarks and light-flavour quarks, as well as decays to taus and electrons, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm, can be used for algorithm training and performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/800xxx/800030/mc.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.py
  use_for_training: ['pflow']
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-ext-af3:
  dsid: 800030
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to b-quarks, c-quarks and light-flavour quarks, as well as decays to taus and electrons, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm, with ATLAS fast simulation AF3, can be used for dedicated studies related to fast detector simulation."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/800xxx/800030/mc.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-ext-norad:
  dsid: 800030
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to b-quarks, c-quarks and light-flavour quarks, as well as decays to taus and electrons, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm. Radiation damage simulation is turned off for this sample (default for mc21 is that it is turned on), to perform investigations in Run-3/mc21 performance at high-pt."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/800xxx/800030/mc.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-ext-highpileup:
  dsid: 800030
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to b-quarks, c-quarks and light-flavour quarks, as well as decays to taus and electrons, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm. Higher pile-up is simulated for performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/800xxx/800030/mc.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-ext-noibl:
  dsid: 800030
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to b-quarks, c-quarks and light-flavour quarks, as well as decays to taus and electrons, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm. The inner B-layer is removed for performance studies."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/800xxx/800030/mc.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-mg5h7:
  dsid: 500567
  description: "MG5+Herwig7 Z' from (with mass of 6 TeV, 50% width and flat pt spectrum) with all allowed decays, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm. UFO model: http://feynrules.irmp.ucl.ac.be/wiki/Top-Philic-Zprime"
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/500xxx/500567/mc.MGH7EG_NNPDF23ME_Zprime.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-mg5py8:
  dsid: 500568
  description: "MG5+Pythia8 Z' (with mass of 6 TeV, 50% width and flat pt spectrum) with all allowed decays, TestHepMC MaxVtxDisp extended from 1000mm to 1500mm. UFO model: http://feynrules.irmp.ucl.ac.be/wiki/Top-Philic-Zprime"
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/500xxx/500567/mc.MGH7EG_NNPDF23ME_Zprime.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zprime-tt:
  dsid: 426345
  description: "Pythia8 Z' (with mass of 4 TeV and flat pt spectrum) with decays to top quark pairs, can be used for studies of high-pt b-jets."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID426xxx/MC15.426345.Pythia8EvtGen_A14NNPDF23LO_Zprime_tt_flatpT.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

graviton:
  dsid: 504648
  description: "MG5+Pythia8 KK spin-2 graviton (with mass of 3 TeV, 50% width) with decays to b-quarks, c-quarks and light-flavour quarks, as well as decays to tau leptons."
  joboption: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/504xxx/504648/mc.aMCPy8EG_A14NNPDF23LO_Hjj_m3000w50.py
  use_for_training: ['vr']
  use_for_workingpoint: []
  use_for_mcmc: []

dijet:
  dsid: [364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712]
  description: "Pythia8 dijet sample slices (JZ*WithSW) for performance studies."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364700.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364708.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364710.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364711.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11WithSW.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID364xxx/MC15.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

znunujets-sherpa2211:
  dsid: [700335, 700336, 700337]
  description: "Sherpa 2.2.11 Z+jets samples with Z decay to neutrinos."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700335/mc.Sh_2211_Znunu_pTV2_BFilter.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700336/mc.Sh_2211_Znunu_pTV2_CFilterBVeto.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700337/mc.Sh_2211_Znunu_pTV2_CVetoBVeto.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zeejets-sherpa2211:
  dsid: [700320, 700321, 700322]
  description: "Sherpa 2.2.11 Z+jets samples with Z decay to electrons."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700320/mc.Sh_2211_Zee_maxHTpTV2_BFilter.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700321/mc.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700322/mc.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

zmumujets-sherpa2211:
  dsid: [700323, 700324, 700325]
  description: "Sherpa 2.2.11 Z+jets samples with Z decay to muons."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700323/mc.Sh_2211_Zmumu_maxHTpTV2_BFilter.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700324/mc.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700325/mc.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

ztautauHHjets-sherpa2211:
  dsid: [700332, 700333, 700334]
  description: "Sherpa 2.2.11 Z+jets samples with Z decay to taus (hadronic + hadronic tau decay)."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700332/mc.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700333/mc.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700334/mc.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: []

ttbar-amcatnlopy8:
  dsid: [410464, 410465]
  description: "aMC@NLO+Pythia8 top quark pairs with single-lepton and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-amcatnlh7:
  dsid: [412116, 412117]
  description: "aMC@NLO+Herwig7 top quark pairs with single-lepton and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID412xxx/MC15.412116.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID412xxx/MC15.412117.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-powhegh704:
  dsid: [410557, 410558]
  description: "Powheg+Herwig7.0.4 top quark pairs with single-lepton and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-powhegh713:
  dsid: [411233, 411234]
  description: "Powheg+Herwig7.1.3 top quark pairs with single-lepton and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID411xxx/MC15.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID411xxx/MC15.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-powhegh721:
  dsid: [600666, 600667]
  description: "Powheg+Herwig7.2.1 top quark pairs with single-lepton and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/600xxx/600666/mc.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/600xxx/600667/mc.PhH7EG_H7UE_tt_hdamp258p75_721_dil.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-sherpa221:
  dsid: [410250, 410251, 410252]
  description: "Sherpa 2.2.1 top quark pairs with lepton+jets and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410250.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410251.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO.py
    - https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID410xxx/MC15.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-sherpa228:
  dsid: [421152, 421153, 421154]
  description: "Sherpa 2.2.8 top quark pairs with lepton+jets and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/421xxx/421152/mc.Sh_N30NNLO_ttbar_SingleLeptonP_MEPS_NLO_BBMODE5.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/421xxx/421153/mc.Sh_N30NNLO_ttbar_SingleLeptonM_MEPS_NLO_BBMODE5.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/421xxx/421154/mc.Sh_N30NNLO_ttbar_dilepton_MEPS_NLO_BBMODE5.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-sherpa2210:
  dsid: [700122, 700123, 700124]
  description: "Sherpa 2.2.10 top quark pairs with lepton+jets and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700122/mc.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700123/mc.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700124/mc.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']

ttbar-sherpa2212:
  dsid: [700660, 700661, 700662]
  description: "Sherpa 2.2.12 top quark pairs with lepton+jets and di-lepton decays."
  joboption:
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700660/mc.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700661/mc.Sh_2212_ttbar_SingleLeptonM_maxHTavrgTopPT.py
    - https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700662/mc.Sh_2212_ttbar_SingleLeptonP_maxHTavrgTopPT.py
  use_for_training: []
  use_for_workingpoint: []
  use_for_mcmc: ['pflow', 'vr']
